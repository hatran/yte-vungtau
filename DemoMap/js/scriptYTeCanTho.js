﻿var lat = 10.76994;
var lon = 106.69938;
var map = new L.Map('map', {
    minZoom: 8
});

// create a new tile layer
var tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl, {
        attribution: '', //'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 20
    });

map.addLayer(layer);

map.setView([lat, lon], 17);

var hospitalIcon = L.icon({
    iconUrl: 'images/pin-hopital.png',
    iconSize: [30, 30], // size of the icon
});

var FireIcon = L.icon({
    iconUrl: 'images/fire-2-32_2.gif',
    iconSize: [36, 36], // size of the icon
});

var carIcon = L.icon({
    iconUrl: 'images/ambulance.png',
    iconSize: [36, 36], // size of the icon
});

var lstHospital = data;
// console.log(lstHospital);
var color = null;
var lstColor = [{
        "color": "#003366",
    },
    {
        "color": "#0000FF"
    },
    {
        "color": "#33CC33"
    },
    {
        "color": "#990033"
    },
    {
        "color": "#FF66FF"
    },
    {
        "color": "#CD853F"
    },
    {
        "color": "#CDCD00"
    },
    {
        "color": "#8B658B"
    },
    {
        "color": "#5D478B"
    }
];

var xecuuthuong1 = [
    [lstHospital[0].lat, lstHospital[0].lon],
    [10.76773,106.69711],
    [10.76714,106.69741],
    [10.76768,106.69801],
    [10.76804,106.69844],
    [10.76842,106.69886],
    [10.76929,106.69976],
    [lat, lon]
];
var xecuuthuong2 = [
    [lstHospital[1].lat, lstHospital[1].lon],
    [10.77166,106.69953],
    [10.77161,106.70050],
    [10.77179,106.70076],
    [10.77117,106.70106],
    [10.77068,106.70128],
    [10.76929,106.69977],
    [lat, lon]
];
var xecuuthuong3 = [
    [lstHospital[2].lat, lstHospital[2].lon],
    [10.76546,106.70200],
    [10.76507,106.70165],
    [10.76727,106.69979],
    [10.76842,106.69885],
    [10.76842,106.69886],
    [10.76928,106.69977],
    [lat, lon]
];

var marker = L.marker([lat, lon], { icon: FireIcon }).bindPopup('<p style="font-size:20px;">Hiện Trường: <span style="color:blue">Bảo Tàng Mỹ Thuật</span></p>').addTo(map);
marker.openPopup();
var route = null;
var maker = null;


var markerCT1 = L.Marker.movingMarker(xecuuthuong1,
    [10000, 10000, 10000, 10000, 10000, 10000, 10000], { autostart: true, loop: false, icon: carIcon }).addTo(map);

markerCT1.loops = 0;
markerCT1.bindPopup();
markerCT1.on('mouseover', function(e) {
    //open popup;
    var popup = L.popup()
        .setLatLng(e.latlng)
        .setContent('Xe cứu thương biển số 14N - 90378 thuộc: Bệnh viện đa khoa tỉnh Quảng Ninh<br>Lái xe: Bùi Đức Toàn<br>Bác sỹ: Nông Văn Dũng<br>Điều dưỡng: Phạm Thái Dương<br>Địa chỉ: Chợ Hà Lâm<br>Bệnh nhân: Nguyên Văn Đạt <br>Còn <strong>15</strong> phút nữa hiện trường')
        .openOn(map);
});
markerCT1.start();

var markerCT2 = L.Marker.movingMarker(xecuuthuong2,
    [10000, 10000, 10000, 10000, 10000, 10000, 10000], { autostart: true, loop: false, icon: carIcon }).addTo(map);

markerCT2.loops = 0;
markerCT2.bindPopup();
markerCT2.on('mouseover', function(e) {
    //open popup;
    var popup = L.popup()
        .setLatLng(e.latlng)
        .setContent('Xe cứu thương biển số 14N - 76904 thuộc: Trung tâm y tế Bãi Cháy<br>Lái xe: Ngô Xuân Hải<br>Bác sỹ: Lưu T. Quỳnh Nga<br>Điều dưỡng: Phạm Thị Thương<br>Địa chỉ: Chợ Hà Lâm<br>Bệnh nhân: Nguyên Văn Long <br>Còn <strong>10</strong> phút nữa hiện trường')
        .openOn(map);
});
markerCT2.start();

var markerCT3 = L.Marker.movingMarker(xecuuthuong3,
    [10000, 10000, 10000, 10000, 10000, 10000, 10000], { autostart: true, loop: false, icon: carIcon }).addTo(map);

markerCT3.loops = 0;
markerCT3.bindPopup();
markerCT3.on('mouseover', function(e) {
    //open popup;
    var popup = L.popup()
        .setLatLng(e.latlng)
        .setContent('Xe cứu thương biển số 14N - 76904 thuộc: Trung tâm y tế Bãi Cháy<br>Lái xe: Ngô Xuân Hải<br>Bác sỹ: Lưu T. Quỳnh Nga<br>Điều dưỡng: Phạm Thị Thương<br>Địa chỉ: Chợ Hà Lâm<br>Bệnh nhân: Nguyên Văn Long <br>Còn <strong>10</strong> phút nữa hiện trường')
        .openOn(map);
});
markerCT3.start();

// var route1 = new L.Routing.control({
//     waypoints: [
//         L.latLng(20.44136, 106.33847),
//         L.latLng(20.44191, 106.33907),
//         L.latLng(20.44303, 106.33849),
//         L.latLng(20.44415, 106.33794),
//         L.latLng(20.44535, 106.33724),
//         L.latLng(20.44482, 106.33652),
//         L.latLng(lat, lon)
//     ],
//     addWaypoints: false,
//     lineOptions: {
//         styles: [{ color: "blue", opacity: 1, weight: 5 }]
//     },
//     createMarker: function() { return null; },
//     draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);

// var route2 = new L.Routing.control({
//     waypoints: [
//         L.latLng(20.44840, 106.34537),
//         L.latLng(20.44887, 106.34480),
//         L.latLng(20.44799, 106.34347),
//         L.latLng(20.44433, 106.33795),
//         L.latLng(20.44536, 106.33726),
//         L.latLng(20.44482, 106.33652),
//         L.latLng(lat, lon)
//     ],
//     addWaypoints: false,
//     lineOptions: {
//         styles: [{ color: "blue", opacity: 1, weight: 5 }]
//     },
//     createMarker: function() { return null; },
//     draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function openModal(idModal) {
    document.getElementById(idModal).style.display = "block";
}